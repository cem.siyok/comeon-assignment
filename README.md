Spring of Emails Assignment:

Must-haves
* Fetch data from external urls
* Drop invalid emails and unwanted domains (wanted domains are comenon.com and cherry.se)
* Count and persist how many times an email is encountered, in five minutes batches
* After five minutes of first data feed, persist emails/counts and close batch. Start the new batch at next incoming data
* Provide API to retrieve the emails and corresponding counts
* Provide API to retrieve an email’s count
* Provide API for CRUD operations on a single email resource