package com.comeon.emailapi.service;

import com.comeon.emailapi.dataaccessobject.EmailRepository;
import com.comeon.emailapi.domainobject.EmailDO;
import com.comeon.emailapi.exception.ConstraintsViolationException;
import com.comeon.emailapi.exception.EntityNotFoundException;
import com.comeon.emailapi.exception.InvalidEmailException;
import com.comeon.emailapi.model.EmailUploadRequest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.ResponseEntity;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.client.RestTemplate;

import java.util.*;

import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class DefaultEmailServiceTest {

    @InjectMocks
    private DefaultEmailService defaultEmailService;

    @Mock
    private EmailRepository emailRepository;

    @Mock
    private RestTemplate restTemplate;

    @Test
    public void it_should_bulk_upload() {
        // given
        EmailUploadRequest dataSet1 = new EmailUploadRequest();
        dataSet1.setEmail(Arrays.asList("email_1@comeon.com", "email_2@comeon.com", "email_3@comeon.com", "email_4@comeon.com", "email_5@comeon.com"));
        dataSet1.setUrl(Arrays.asList("url1", "url2", "url3"));

        EmailUploadRequest dataSet2 = new EmailUploadRequest();
        dataSet2.setEmail(Arrays.asList("email_1@comeon.com", "email_2@comeon.com", "email_3@comeon.com", "email_4@comeon.com"));
        dataSet2.setUrl(Collections.singletonList("url4"));

        EmailUploadRequest dataSet3 = new EmailUploadRequest();
        dataSet3.setEmail(Arrays.asList("email_1@comeon.com", "email_2@comeon.com", "email_3@gmail.com"));

        when(restTemplate.getForEntity("url1", EmailUploadRequest.class)).thenReturn(ResponseEntity.ok(dataSet2));
        when(restTemplate.getForEntity("url2", EmailUploadRequest.class)).thenReturn(ResponseEntity.ok(dataSet2));
        when(restTemplate.getForEntity("url3", EmailUploadRequest.class)).thenReturn(ResponseEntity.ok(dataSet2));
        when(restTemplate.getForEntity("url4", EmailUploadRequest.class)).thenReturn(ResponseEntity.ok(dataSet3));

        // when
        defaultEmailService.bulkUpload(dataSet1);

        // then
        Map<String, Long> emailCountCache = (Map<String, Long>) ReflectionTestUtils.getField(defaultEmailService, "emailCountCache");
        assertThat(emailCountCache.get("email_1@comeon.com")).isEqualTo(7L);
        assertThat(emailCountCache.get("email_2@comeon.com")).isEqualTo(7L);
        assertThat(emailCountCache.get("email_3@comeon.com")).isEqualTo(4L);
        assertThat(emailCountCache.get("email_4@comeon.com")).isEqualTo(4L);
        assertThat(emailCountCache.get("email_5@comeon.com")).isEqualTo(1L);
        assertThat(emailCountCache.get("email_3@gmail.com")).isNull();
    }

    @Test
    public void it_should_create_new_email() throws InvalidEmailException, ConstraintsViolationException {
        // given
        EmailDO expected = new EmailDO("email_1@comeon.com");
        when(emailRepository.save(any(EmailDO.class))).thenReturn(expected);

        // when
        EmailDO actual = defaultEmailService.createEmail(new EmailDO("email_1@comeon.com"));

        // then
        assertThat(expected).isEqualTo(actual);
    }

    @Test
    public void it_should_persist_cache() {
        // given
        HashMap<String, Long> cache = new HashMap<>();
        cache.put("email_1@comeon.com", 10L);
        ReflectionTestUtils.setField(defaultEmailService, "emailCountCache", cache);

        EmailDO email = new EmailDO("email_1@comeon.com", 5L);
        when(emailRepository.findByEmail(anyString())).thenReturn(Optional.of(email));

        // when
        defaultEmailService.persistCache();

        // then
        assertThat(email.getCount()).isEqualTo(15L);

        verify(emailRepository).save(email);
    }

    @Test
    public void it_should_delete_email() throws EntityNotFoundException {
        // given
        EmailDO email = new EmailDO("email_1@comeon.com", 10L);
        when(emailRepository.findById(1L)).thenReturn(Optional.of(email));

        // when
        defaultEmailService.deleteEmail(1L);

        // then
        assertThat(email.getDeleted()).isTrue();
    }

    @Test
    public void it_should_update_count_for_given_email() throws EntityNotFoundException {
        // given
        EmailDO email = new EmailDO("email_1@comeon.com", 10L);
        when(emailRepository.findById(1L)).thenReturn(Optional.of(email));

        // when
        defaultEmailService.updateEmailCount(1L, 20L);

        // then
        assertThat(email.getCount()).isEqualTo(20L);
    }

    @Test
    public void it_should_find_email() throws EntityNotFoundException {
        // given
        EmailDO expected = new EmailDO("email_1@comeon.com", 10L);
        when(emailRepository.findById(1L)).thenReturn(Optional.of(expected));

        // when
        EmailDO actual = defaultEmailService.find(1L);

        // then
        assertThat(actual).isEqualTo(expected);
    }

    @Test(expected = EntityNotFoundException.class)
    public void it_throw_exception_when_no_entity_find_with_given_id() throws EntityNotFoundException {
        // given
        when(emailRepository.findById(1L)).thenReturn(Optional.empty());

        // when
        defaultEmailService.find(1L);

        // then
    }

}
