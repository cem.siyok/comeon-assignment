package com.comeon.emailapi.service;

import com.comeon.emailapi.domainobject.EmailDO;
import com.comeon.emailapi.exception.ConstraintsViolationException;
import com.comeon.emailapi.exception.EntityNotFoundException;
import com.comeon.emailapi.exception.InvalidEmailException;
import com.comeon.emailapi.model.EmailFilterRequest;
import com.comeon.emailapi.model.EmailUploadRequest;
import org.springframework.data.domain.Page;

public interface EmailService {

    void bulkUpload(EmailUploadRequest emailUploadRequest);

    Page<EmailDO> filter(EmailFilterRequest emailFilterRequest);

    EmailDO find(Long emailId) throws EntityNotFoundException;

    EmailDO createEmail(EmailDO emailDO) throws ConstraintsViolationException, InvalidEmailException;

    void deleteEmail(Long emailId) throws EntityNotFoundException;

    void updateEmailCount(Long emailId, Long count) throws EntityNotFoundException;
}
