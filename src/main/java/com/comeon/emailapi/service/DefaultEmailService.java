package com.comeon.emailapi.service;

import com.comeon.emailapi.dataaccessobject.EmailRepository;
import com.comeon.emailapi.dataaccessobject.specification.EmailFilterSpecifications;
import com.comeon.emailapi.domainobject.EmailDO;
import com.comeon.emailapi.exception.ConstraintsViolationException;
import com.comeon.emailapi.exception.EntityNotFoundException;
import com.comeon.emailapi.exception.InvalidEmailException;
import com.comeon.emailapi.model.EmailFilterRequest;
import com.comeon.emailapi.model.EmailUploadRequest;
import com.comeon.emailapi.validation.EmailValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

/**
 * Service to encapsulate the link between DAO and controller and to have business logic for some email specific things.
 * <p/>
 */
@Service
public class DefaultEmailService implements EmailService {

    private static final Logger LOG = LoggerFactory.getLogger(DefaultEmailService.class);

    private final EmailRepository emailRepository;
    private final RestTemplate restTemplate;
    private final Map<String, Long> emailCountCache = new ConcurrentHashMap<>();

    public DefaultEmailService(EmailRepository emailRepository,
                               RestTemplate restTemplate) {
        this.emailRepository = emailRepository;
        this.restTemplate = restTemplate;
    }

    @Override
    public EmailDO find(Long emailId) throws EntityNotFoundException {
        return findEmailChecked(emailId);
    }

    @Override
    public EmailDO createEmail(EmailDO emailDO) throws ConstraintsViolationException, InvalidEmailException {
        EmailDO email;
        try {
            if (!EmailValidator.validate(emailDO.getEmail())) {
                throw new InvalidEmailException("Invalid emails and domains except comenon.com and cherry.se are not acceptable");
            }
            email = emailRepository.save(emailDO);
        } catch (DataIntegrityViolationException ex) {
            LOG.warn("ConstraintsViolationException while creating a email: {}", emailDO, ex);
            throw new ConstraintsViolationException(ex.getMessage());
        }
        return email;
    }

    @Override
    @Transactional
    public void deleteEmail(Long emailId) throws EntityNotFoundException {
        EmailDO emailDO = findEmailChecked(emailId);
        emailDO.setDeleted(true);
    }

    @Override
    @Transactional
    public void updateEmailCount(Long emailId, Long count) throws EntityNotFoundException {
        EmailDO email = findEmailChecked(emailId);
        email.setCount(count);
    }

    @Override
    public Page<EmailDO> filter(EmailFilterRequest emailFilterRequest) {
        PageRequest pageRequest = PageRequest.of
                (
                        emailFilterRequest.getPage(),
                        emailFilterRequest.getSize(),
                        Sort.by(Sort.Direction.DESC, "dateCreated")
                );
        return emailRepository.findAll(EmailFilterSpecifications.getFilterQuery(emailFilterRequest), pageRequest);
    }

    @Override
    public void bulkUpload(EmailUploadRequest emailUploadRequest) {
        feedCache(Collections.singletonList(emailUploadRequest));
    }

    private void feedCache(List<EmailUploadRequest> emailUploadRequests) {
        for (EmailUploadRequest dataSet : emailUploadRequests) {
            dataSet.getEmail()
                    .stream()
                    .filter(EmailValidator::validate)
                    .forEach(email -> {
                        emailCountCache.putIfAbsent(email, 0L);
                        emailCountCache.computeIfPresent(email, (key, lastCount) -> lastCount + 1L);
                    });
            this.feedCache(getEmailUploadRequest(dataSet.getUrl()));
        }
    }

    private List<EmailUploadRequest> getEmailUploadRequest(List<String> urls) {
        List emailUploadRequests = Collections.EMPTY_LIST;
        try {
            emailUploadRequests = urls.stream().map(url -> restTemplate.getForEntity(url, EmailUploadRequest.class).getBody()).collect(Collectors.toList());
        } catch (HttpClientErrorException ex) {
            LOG.warn("HttpClientErrorException has occurred when fetching new dataset from an external url.", ex);
        }
        return emailUploadRequests;
    }

    private EmailDO findEmailChecked(Long emailId) throws EntityNotFoundException {
        return emailRepository.findById(emailId).orElseThrow(() -> new EntityNotFoundException("Could not find entity with id: " + emailId));
    }

    @Scheduled(fixedDelayString = "50000")
    public void persistCache() {
        for (Map.Entry<String, Long> eachEntry : emailCountCache.entrySet()) {
            Long newCount = eachEntry.getValue();
            String email = eachEntry.getKey();
            createOrUpdateEmail(email, newCount);
            emailCountCache.computeIfPresent(email, (key, lastCount) -> lastCount - newCount);
        }
        emailCountCache.entrySet().removeIf(entry -> entry.getValue() == 0L);
    }

    @Transactional
    public void createOrUpdateEmail(String email, Long count) {
        EmailDO emailDO = emailRepository.findByEmail(email).orElse(new EmailDO(email));
        emailDO.increaseCountBy(count);
        emailRepository.save(emailDO);
    }
}
