package com.comeon.emailapi.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Email is not valid.")
public class InvalidEmailException extends Exception {

    private static final long serialVersionUID = 3499747994706251060L;

    public InvalidEmailException(String message) {
        super(message);
    }

}
