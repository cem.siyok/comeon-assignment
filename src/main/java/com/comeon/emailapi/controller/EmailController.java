package com.comeon.emailapi.controller;

import com.comeon.emailapi.controller.mapper.EmailMapper;
import com.comeon.emailapi.datatransferobject.EmailDTO;
import com.comeon.emailapi.domainobject.EmailDO;
import com.comeon.emailapi.exception.ConstraintsViolationException;
import com.comeon.emailapi.exception.EntityNotFoundException;
import com.comeon.emailapi.exception.InvalidEmailException;
import com.comeon.emailapi.model.EmailFilterRequest;
import com.comeon.emailapi.model.EmailUploadRequest;
import com.comeon.emailapi.service.EmailService;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/v1/emails")
public class EmailController {

    private final EmailService emailService;

    public EmailController(EmailService emailService) {
        this.emailService = emailService;
    }

    @PostMapping(value = "/bulk", consumes = MediaType.APPLICATION_XML_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public void bulkUploadEmails(@RequestBody EmailUploadRequest emailUploadRequest) {
        emailService.bulkUpload(emailUploadRequest);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public EmailDTO createEmail(@Valid @RequestBody EmailDTO emailDTO) throws ConstraintsViolationException, InvalidEmailException {
        EmailDO emailDO = EmailMapper.makeEmailDO(emailDTO);
        return EmailMapper.makeEmailDTO(emailService.createEmail(emailDO));
    }

    @PatchMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void updateEmailCount(@PathVariable Long id, @RequestParam Long count) throws EntityNotFoundException {
        emailService.updateEmailCount(id, count);
    }

    @GetMapping("/filter")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<List<EmailDTO>> filter(@ModelAttribute EmailFilterRequest emailFilterRequest) {
        Page<EmailDO> pageResult = emailService.filter(emailFilterRequest);
        return ResponseEntity.ok()
                .header("Pagination-Page", String.valueOf(emailFilterRequest.getPage()))
                .header("Pagination-Limit", String.valueOf(emailFilterRequest.getSize()))
                .header("Pagination-Count", String.valueOf(pageResult.getTotalElements()))
                .body(EmailMapper.makeDTOList(pageResult.getContent()));
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public EmailDTO getEmail(@PathVariable Long id) throws EntityNotFoundException {
        return EmailMapper.makeEmailDTO(emailService.find(id));
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void deleteEmail(@PathVariable Long id) throws EntityNotFoundException {
        emailService.deleteEmail(id);
    }

}
