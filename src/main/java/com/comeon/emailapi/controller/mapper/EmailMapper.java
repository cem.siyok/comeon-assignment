package com.comeon.emailapi.controller.mapper;

import com.comeon.emailapi.datatransferobject.EmailDTO;
import com.comeon.emailapi.domainobject.EmailDO;

import java.util.List;
import java.util.stream.Collectors;

public class EmailMapper {

    public static EmailDO makeEmailDO(EmailDTO emailDTO) {
        return new EmailDO(emailDTO.getEmail(), emailDTO.getCount());
    }

    public static EmailDTO makeEmailDTO(EmailDO emailDO) {
        return EmailDTO.builder()
                .id(emailDO.getId())
                .email(emailDO.getEmail())
                .count(emailDO.getCount())
                .build();
    }

    public static List<EmailDTO> makeDTOList(List<EmailDO> emailDOList) {
        return emailDOList.stream().map(EmailMapper::makeEmailDTO).collect(Collectors.toList());
    }

}
