package com.comeon.emailapi.model;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@XmlRootElement
public class EmailUploadRequest {

    @JacksonXmlElementWrapper(localName = "emails")
    private List<String> email = new ArrayList<>();

    @JacksonXmlElementWrapper(localName = "resources")
    private List<String> url = new ArrayList<>();

    public List<String> getEmail() {
        return Optional.ofNullable(email).orElse(Collections.emptyList());
    }

    public void setEmail(List<String> email) {
        this.email = email;
    }

    public List<String> getUrl() {
        return Optional.ofNullable(url).orElse(Collections.emptyList());
    }

    public void setUrl(List<String> url) {
        this.url = url;
    }
}
