package com.comeon.emailapi.datatransferobject;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

public class EmailDTO {

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Long id;

    @NotNull(message = "Email can not be null")
    @Email(message = "Email should be valid")
    private String email;

    @Range(min = 1, message = "Count can be negative")
    @NotNull(message = "Count can not be null")
    private Long count;

    public Long getId() {
        return id;
    }

    @JsonIgnore
    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder {
        private EmailDTO emailDTO;

        private Builder() {
            emailDTO = new EmailDTO();
        }

        public Builder id(Long id) {
            emailDTO.setId(id);
            return this;
        }

        public Builder email(String email) {
            emailDTO.setEmail(email);
            return this;
        }

        public Builder count(Long count) {
            emailDTO.setCount(count);
            return this;
        }

        public EmailDTO build() {
            return emailDTO;
        }
    }
}
