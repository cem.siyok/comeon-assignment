package com.comeon.emailapi.dataaccessobject;

import com.comeon.emailapi.domainobject.EmailDO;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface EmailRepository extends CrudRepository<EmailDO, Long>, JpaSpecificationExecutor<EmailDO> {

    Optional<EmailDO> findByEmail(String email);

}
