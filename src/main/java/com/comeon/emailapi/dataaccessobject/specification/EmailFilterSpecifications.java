package com.comeon.emailapi.dataaccessobject.specification;

import com.comeon.emailapi.domainobject.EmailDO;
import com.comeon.emailapi.model.EmailFilterRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.util.StringUtils;

import javax.persistence.criteria.Predicate;
import java.util.Objects;

public final class EmailFilterSpecifications {

    public static Specification<EmailDO> getFilterQuery(EmailFilterRequest emailFilterRequest) {
        return (root, query, cb) -> {
            Predicate conjuction = cb.isFalse(root.get("deleted"));

            if (!StringUtils.isEmpty(emailFilterRequest.getEmail())) {
                conjuction = cb.and(conjuction, cb.equal(root.get("email"), emailFilterRequest.getEmail()));
            }
            if (Objects.nonNull(emailFilterRequest.getCount())) {
                conjuction = cb.and(conjuction, cb.equal(root.get("count"), emailFilterRequest.getCount()));
            }
            return conjuction;
        };
    }

}
